#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; # -> Win
; ! -> Alt
; ^ -> Ctrl
; + -> Shift

; 键位映射修改
; Alt与Ctrl对调, 建议通过注册表修改
; LAlt::LCtrl
; LCtrl::LAlt

; 修改鼠标滚轮方向, 建议通过注册表修改
; WheelDown::WheelUp
; WheelUp::WheelDown

; 退出
; Esc :: CapsLock + y 
CapsLock & y::Send, {Esc}

; 通用后退与前进
; Alt + Left/Right :: CapsLock + 鼠标左键/右键
CapsLock & LButton::Send, !{Left}
CapsLock & RButton::Send, !{Right}

; 光标移动  
; 按字符移动(模拟Vim)上下左右
; 左/下/上/右 :: CapsLock + h/j/k/l
CapsLock & h::Send, {Left}
CapsLock & j::Send, {Down}
CapsLock & k::Send, {Up}
CapsLock & l::Send, {Right}

; 按单词移动
; Ctrl + 左/右 :: CapsLock + u/i
CapsLock & u::Send, ^{Left}
CapsLock & i::Send, ^{Right}

; 移动到行首/行尾
; Home/End :: CapsLock + o/p
CapsLock & o::Send, {Home}
CapsLock & p::Send, {End}

; 按页移动(翻页)
; PgUp/PgDn :: CapsLock + [或]
CapsLock & [::Send, {PgUp}
CapsLock & ]::Send, {PgDn}

; 移动到文档首/尾
; Ctrl + Home/End :: CapsLock + d/f
CapsLock & d::Send, ^{Home}
CapsLock & f::Send, ^{End}
CapsLock & 9::Send, ^{Home}
CapsLock & 0::Send, ^{End}


; 退格删除/向前删除, 按字符
; Backspace/Delete :: CapsLock + n/m
; 退格删除/向前删除, 按单词
; Ctrl + Backspace/Delete :: CapsLock + ,/.
CapsLock & n::Send, {BackSpace}
CapsLock & m::Send, {Delete}
CapsLock & ,::Send, ^{BackSpace}
CapsLock & .::Send, ^{Delete}

; 文本选择
; 按字符选中
; Shift + 左/下/上/右 :: Shift + h/j/k/l
; 按单词选中
; Shift + Ctrl + 左/下/上/右 :: Shift Ctrl + h/j/k/l
+h::Send, +{Left}
+j::Send, +{Down}
+k::Send, +{Up}
+l::Send, +{Right}
+^h::Send, +^{Left}
+^j::Send, +^{Down}
+^k::Send, +^{Up}
+^l::Send, +^{Right}

; 选中到行首/行尾
; Shift + Home/End :: Shift + Ctrl + ;/'
+^;::Send, +{Home}
+^'::Send, +{End}



